# Introduction

This is a sample project to reproduce the problem described on [this post on stackoverflow](https://stackoverflow.com/questions/54486142/how-to-use-hibernates-filter-annotation-in-an-embeddedid-property).

# How to use it

Just execute the main class "Example". For this example I'm testing with postgres 9.4.

# Expected behavior

print at stdout:

```
right
```

# Actual behavior

print at stdout:

```
wrong
right
```
