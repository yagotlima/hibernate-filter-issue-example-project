package com.example.issue;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@FilterDef(name="dateDueFilter", parameters= {
	@ParamDef( name="dateDue", type="date" ),
})
@Filters( {
	@Filter(name="dateDueFilter", condition="date_due = :dateDue"),
})
public class CarRent implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CarRentKey carRentKey;

	@MapsId("clientId")
	@ManyToOne()
	@JoinColumn(name = "client_id", nullable = false, insertable = false, updatable = false)
	private Client client;

	@MapsId("carId")
	@ManyToOne()
	@JoinColumn(name = "car_id", nullable = false, insertable = false, updatable = false)
	private Car car;

	@Basic(optional = false)
	private String bar;

	public CarRent() {
	}

	public CarRent(Client client, Car car, Date dateDue, String bar) {
		this.client = client;
		this.car = car;
		this.bar = bar;
		this.carRentKey = new CarRentKey();
		this.carRentKey.setDateDue(dateDue);
	}

	public CarRentKey getCarRentKey() {
		return carRentKey;
	}

	public void setCarRentKey(CarRentKey carRentKey) {
		this.carRentKey = carRentKey;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(carRentKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CarRent))
			return false;

		return Objects.equals(carRentKey, ((CarRent) obj).carRentKey);
	}
}
