package com.example.issue;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name = "client_id", sequenceName = "client_id_seq", allocationSize = 1)
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id")
	@Basic(optional = false)
	private Integer id;

	@Basic(optional = false)
	private String name;

	@OneToMany(mappedBy = "client")
	private List<CarRent> rentHistory;
	
	public Client() {
	}

	public Client(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CarRent> getRentHistory() {
		return rentHistory;
	}

	public void setRentHistory(List<CarRent> rentHistory) {
		this.rentHistory = rentHistory;
	}

}
