package com.example.issue;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.hibernate.Session;

public class Example {
	private EntityManager em;
	private Date exampleDate = new GregorianCalendar(2019, 1, 1).getTime();

	public static void main(String[] args) {
		new Example().test();

		System.exit(0);
	}

	public Example() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EXAMPLE_PU");
		em = emf.createEntityManager();
	}

	public void test() {
		if (canSeed())
			seed();

		Session hibernateSession = em.unwrap(Session.class);
		hibernateSession.enableFilter("dateDueFilter").setParameter("dateDue", exampleDate);
		Query q = em.createQuery("select distinct(cl) from Client cl left outer join fetch cl.rentHistory");
		@SuppressWarnings("unchecked")
		List<Client> clientList = q.getResultList();
		
		hibernateSession.disableFilter("dateDueFilter");

		clientList.stream().forEach(client -> {
			client.getRentHistory().stream().forEach(carRent -> {
				System.out.println(carRent.getBar());
			});
		});
	}

	private boolean canSeed() {
		return (Long) em.createQuery("select count(*) from CarRent carRent").getSingleResult() == 0l;
	}

	private void seed() {
		em.getTransaction().begin();
		Car car = new Car("car1");
		em.persist(car);
		Client client = new Client("clent1");
		em.persist(client);
		CarRent carRent = new CarRent(client, car, new Date(), "wrong");
		em.persist(carRent);
		carRent = new CarRent(client, car, exampleDate, "right");
		em.persist(carRent);
		em.getTransaction().commit();
	}

}
