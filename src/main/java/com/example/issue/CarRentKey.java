package com.example.issue;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CarRentKey implements Serializable {
	private static final long serialVersionUID = 1L;

	private int clientId;
	private int carId;

	@Column(name = "date_due")
	private Date dateDue;

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getCarId() {
		return carId;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public Date getDateDue() {
		return dateDue;
	}

	public void setDateDue(Date dateDue) {
		this.dateDue = dateDue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(clientId, carId, dateDue);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CarRentKey))
			return false;

		CarRentKey other = (CarRentKey) obj;

		return Objects.equals(clientId, other.clientId) && Objects.equals(carId, other.carId)
				&& Objects.equals(dateDue, other.dateDue);
	}
}
